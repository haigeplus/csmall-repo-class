package cn.tedu.mall.order.mapper;

import cn.tedu.mall.pojo.order.model.OmsCart;
import cn.tedu.mall.pojo.order.vo.CartStandardVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OmsCartMapper {

    // 查询(判断)当前登录用户购物车中是否包含指定skuId的商品
    OmsCart selectExistsCart(@Param("userId") Long userId,
                             @Param("skuId") Long skuId);

    // 新增sku信息到购物车表中
    int saveCart(OmsCart omsCart);

    // 修改购物车中sku商品数量的方法
    int updateQuantityById(OmsCart omsCart);

    // 根据当前登录用户id查询此用户购物车中sku信息
    List<CartStandardVO> selectCartByUserId(Long userId);

    // 根据用户选中的购物车商品id,删除购物车商品(支持批量删除)
    int deleteCartsByIds(Long[] ids);

    // 清空当前登录用户的购物车商品
    int deleteCartsByUserId(Long userId);

    // 根据用户Id和skuId删除购物车商品信息的方法(新增订单业务中使用)
    int deleteCartByUserIdAndSkuId(OmsCart omsCart);



}
