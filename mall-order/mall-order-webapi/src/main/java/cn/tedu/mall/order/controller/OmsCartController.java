package cn.tedu.mall.order.controller;

import cn.tedu.mall.common.restful.JsonPage;
import cn.tedu.mall.common.restful.JsonResult;
import cn.tedu.mall.order.service.IOmsCartService;
import cn.tedu.mall.order.utils.WebConsts;
import cn.tedu.mall.pojo.order.dto.CartAddDTO;
import cn.tedu.mall.pojo.order.dto.CartUpdateDTO;
import cn.tedu.mall.pojo.order.vo.CartStandardVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/oms/cart")
@Api(tags = "购物车管理模块")
public class OmsCartController {

    @Autowired
    private IOmsCartService omsCartService;

    @PostMapping("/add")
    @ApiOperation("新增sku信息到购物车")
    // @PreAuthorize会验证当前SpringSecurity上下文中是否包含用户信息
    // 如果没有用户信息会发生401错误,提示没有登录,如果有用户信息
    // 会验证用户是否包含ROLE_user权限
    // 所有酷鲨商城前台登录用户都包含'ROLE_user'权限
    // 所以这个注解本质上作用就是判断用户是否已经登录
    @PreAuthorize("hasAuthority('ROLE_user')")
    // @Validated注解激活SpringValidation框架验证参数中属性值的功能
    // CartAddDTO类中,定义了很多非null属性值,如果有非null的属性值为null
    // 会抛出BindException由全局异常处理类处理
    public JsonResult addCart(@Validated CartAddDTO cartAddDTO){
        omsCartService.addCart(cartAddDTO);
        return JsonResult.ok("新增sku到购物车完成!");
    }

    @GetMapping("/list")
    @ApiOperation("根据用户id分页查询购物车sku列表")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "页码",name="page",example ="1"),
            @ApiImplicitParam(value = "每页条数",name="pageSize",example ="10")
    })
    // 验证用户已登录
    @PreAuthorize("hasAuthority('ROLE_user')")
    public JsonResult<JsonPage<CartStandardVO>> listCartByPage(
            @RequestParam(required = false,
                    defaultValue = WebConsts.DEFAULT_PAGE) Integer page,
            @RequestParam(required = false,
                    defaultValue = WebConsts.DEFAULT_PAGE_SIZE) Integer pageSize
    ){
        JsonPage<CartStandardVO> jsonPage=omsCartService
                .listCarts(page,pageSize);
        return JsonResult.ok(jsonPage);
    }

    @PostMapping("/delete")
    @ApiOperation("根据id数组删除购物车中商品")
    @ApiImplicitParam(value = "要删除的id数组",name="ids",
                        required = true,dataType = "array")
    @PreAuthorize("hasAuthority('ROLE_user')")
    public JsonResult removeCartsByIde(Long[] ids){
        omsCartService.removeCart(ids);
        return JsonResult.ok("删除完成!");
    }

    @PostMapping("/delete/all")
    @ApiOperation("清空当前登录用户的购物车商品")
    // hasRole就是判断当前登录用户是否具备某些角色的判断方式
    // ('user')在底层判断时,会自动添加'ROLE_',既实际判断的仍然是'ROLE_user'
    // hasRole是SpringSecurity框架判断用户角色的特殊写法
    // 所以下面两个@PreAuthorize注解的效力是相同的
    // @PreAuthorize("hasAuthority('ROLE_user')")
    @PreAuthorize("hasRole('user')")
    public JsonResult removeCartsByUserId(){
        omsCartService.removeAllCarts();
        return JsonResult.ok("购物车已清空!");
    }

    @PostMapping("/update/quantity")
    @ApiOperation("修改购物车中sku商品数量")
    @PreAuthorize("hasRole('user')")
    public JsonResult updateQuantity(@Validated CartUpdateDTO cartUpdateDTO){
        omsCartService.updateQuantity(cartUpdateDTO);
        return JsonResult.ok("修改完成!");
    }


}
