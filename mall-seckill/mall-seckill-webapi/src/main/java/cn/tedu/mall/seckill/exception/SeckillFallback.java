package cn.tedu.mall.seckill.exception;

import cn.tedu.mall.common.restful.JsonResult;
import cn.tedu.mall.common.restful.ResponseCode;
import cn.tedu.mall.pojo.seckill.dto.SeckillOrderAddDTO;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.spring.web.json.Json;


// 秒杀业务的降级处理类
@Slf4j
public class SeckillFallback {

    // 降级处理方法格式和限流方法一致,只是额外添加在方法最后的参数类型为Throwable
    public static JsonResult seckillFallback(String randCode,
                                             SeckillOrderAddDTO seckillOrderAddDTO,
                                             Throwable throwable){
        log.error("请求运行过程中发送异常了!!!");
        throwable.printStackTrace();
        return JsonResult.failed(
                ResponseCode.INTERNAL_SERVER_ERROR,
                "发生异常,降级处理:"+throwable.getMessage());
    }

}
