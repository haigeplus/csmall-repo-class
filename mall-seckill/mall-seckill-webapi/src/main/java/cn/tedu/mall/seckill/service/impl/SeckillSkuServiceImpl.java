package cn.tedu.mall.seckill.service.impl;

import cn.tedu.mall.pojo.product.vo.SkuStandardVO;
import cn.tedu.mall.pojo.seckill.model.SeckillSku;
import cn.tedu.mall.pojo.seckill.model.SeckillSpu;
import cn.tedu.mall.pojo.seckill.vo.SeckillSkuVO;
import cn.tedu.mall.product.service.seckill.IForSeckillSkuService;
import cn.tedu.mall.seckill.mapper.SeckillSkuMapper;
import cn.tedu.mall.seckill.service.ISeckillSkuService;
import cn.tedu.mall.seckill.utils.SeckillCacheUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanCursor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class SeckillSkuServiceImpl implements ISeckillSkuService {

    @Autowired
    private SeckillSkuMapper seckillSkuMapper;
    // dubbo调用product模块查询sku常规信息
    @DubboReference
    private IForSeckillSkuService dubboSkuService;
    // 需要将查询出的sku信息保存到Redis
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public List<SeckillSkuVO> listSeckillSkus(Long spuId) {
        // 根据spuId查询sku列表
        List<SeckillSku> seckillSkus=
                seckillSkuMapper.findSeckillSkusBySpuId(spuId);
        List<SeckillSkuVO> seckillSkuVOs=new ArrayList<>();
        // 遍历数据库查询获取的seckillSkus
        for(SeckillSku sku : seckillSkus){
            // 先取出skuId,后面会多次使用到
            Long skuId=sku.getSkuId();
            // 确定当前sku对应的Redis的key
            String skuVOKey= SeckillCacheUtils.getSeckillSkuVOKey(skuId);
            // 声明一个SeckillSkuVO类型对象
            SeckillSkuVO seckillSkuVO=null;
            // 判断Redis中是否有这个key
            if(redisTemplate.hasKey(skuVOKey)){
                seckillSkuVO= (SeckillSkuVO)
                        redisTemplate.boundValueOps(skuVOKey).get();
            }else{
                // Redis中没有这个key,需要从数据库中查询包括常规信息
                // 查询常规信息需要dubbo调用product模块
                SkuStandardVO skuStandardVO = dubboSkuService.getById(skuId);
                // 实例化对象,将常规信息同名属性赋值
                seckillSkuVO=new SeckillSkuVO();
                BeanUtils.copyProperties(skuStandardVO,seckillSkuVO);
                // 手动赋值秒杀信息,秒杀信息都在正在遍历的sku对象中
                seckillSkuVO.setSeckillPrice(sku.getSeckillPrice());
                seckillSkuVO.setStock(sku.getSeckillStock());
                seckillSkuVO.setSeckillLimit(sku.getSeckillLimit());
                // 到此为止seckillSkuVO就既包含常规信息又包含秒杀信息了
                // 保存在Redis中(测试学习阶段还是5分钟)
                redisTemplate.boundValueOps(skuVOKey).set(
                        seckillSkuVO,
                        1000*60*5+ RandomUtils.nextInt(10000),
                        TimeUnit.MILLISECONDS);
            }
            // if-else结束后(但是仍然在sku遍历的循环中)
            // 我们确定seckillSkuVO已经被赋值,之后就将它添加到返回值的集合中
            seckillSkuVOs.add(seckillSkuVO);
        }
        // 最终别忘了返回seckillSkuVOs!!!!!!
        return seckillSkuVOs;
    }
}
