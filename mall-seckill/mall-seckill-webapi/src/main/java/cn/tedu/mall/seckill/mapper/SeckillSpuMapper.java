package cn.tedu.mall.seckill.mapper;

import cn.tedu.mall.pojo.seckill.model.SeckillSpu;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface SeckillSpuMapper {

    // 查询秒杀商品列表
    List<SeckillSpu> findSeckillSpus();

    // 根据给定时间,查询此时间正在进行秒杀的商品
    List<SeckillSpu> findSeckillSpusByTime(LocalDateTime time);

    // 根据spuId,查询spu秒杀信息
    SeckillSpu findSeckillSpuById(Long spuId);

    // 准备给布隆过滤器:查询所有秒杀商品表中商品的spuId,返回Long[]
    Long[] findAllSeckillSpuIds();

}





