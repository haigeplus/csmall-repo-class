package cn.tedu.mall.seckill.exception;

import cn.tedu.mall.common.restful.JsonResult;
import cn.tedu.mall.common.restful.ResponseCode;
import cn.tedu.mall.pojo.seckill.dto.SeckillOrderAddDTO;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;

// 秒杀业务限流异常处理类
@Slf4j
public class SeckillBlockHandler {

    // 声明限流方法,主要是参数要和控制器方法一致,实际开发时要先确定控制器方法参数
    // 参数末尾要添加一个类型为BlockException类型的参数
    // 当限流\降级方法编写在控制器类之外类中时,需要将限流\降级方法定义为static静态
    public static JsonResult seckillBlock(String randCode,
                                          SeckillOrderAddDTO seckillOrderAddDTO,
                                          BlockException e){
        log.error("有一个请求被限流了");
        return JsonResult.failed(
                ResponseCode.INTERNAL_SERVER_ERROR,
                "服务器忙请稍后再试");
    }
}
