package cn.tedu.mall.seckill.timer.config;

import cn.tedu.mall.seckill.timer.job.SeckillBloomInitialJob;
import cn.tedu.mall.seckill.timer.job.SeckillInitialJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfig {

    // 向Spring容器保存预热操作的Job对象
    @Bean
    public JobDetail initJobDetail(){
        return JobBuilder.newJob(SeckillInitialJob.class)
                .withIdentity("initJobDetail")
                .storeDurably()
                .build();
    }

    // 向Spring容器中保存Trigger对象定时触发预热操作
    @Bean
    public Trigger initTrigger(){
        // 12:00  14:00  16:00   18:00   提前5分钟
        // 11:55  13:55  15:55   17:55
        // cron:   0 55 11,13,15,17 * * ?
        // 学习过程中,为了方便测试,我们设计每分钟都进行运行
        CronScheduleBuilder cron=
                CronScheduleBuilder.cronSchedule("0 0/1 * * * ?");
        return TriggerBuilder.newTrigger()
                .forJob(initJobDetail())
                .withSchedule(cron)
                .withIdentity("initTrigger")
                .build();
    }



    // 向Spring容器保存预热操作的Job对象
    @Bean
    public JobDetail bloomInitJobDetail(){
        return JobBuilder.newJob(SeckillBloomInitialJob.class)
                .withIdentity("bloomInitJobDetail")
                .storeDurably()
                .build();
    }

    // 向Spring容器中保存Trigger对象定时触发预热操作
    @Bean
    public Trigger bloomInitTrigger(){
        CronScheduleBuilder cron=
                CronScheduleBuilder.cronSchedule("0 0/1 * * * ?");
        return TriggerBuilder.newTrigger()
                .forJob(bloomInitJobDetail())
                .withSchedule(cron)
                .withIdentity("bloomInitTrigger")
                .build();
    }


}
