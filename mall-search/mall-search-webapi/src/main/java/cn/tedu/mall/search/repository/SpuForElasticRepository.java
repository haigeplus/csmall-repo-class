package cn.tedu.mall.search.repository;

import cn.tedu.mall.pojo.search.entity.SpuForElastic;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

// SpuForElastic操作ES创建的持久层接口
// 继承ElasticsearchRepository后就只能具备了批量新增的功能
@Repository
public interface SpuForElasticRepository extends
                    ElasticsearchRepository<SpuForElastic,Long> {

    // 自定义查询: 查询title字段包含用户输入的指定分词(关键字)的spu信息
    List<SpuForElastic> querySpuForElasticsByTitleMatches(String title);

    @Query("{\n" +
            "    \"bool\": {\n" +
            "      \"should\": [\n" +
            "        { \"match\": { \"name\":  \"?0\" }},\n" +
            "        { \"match\": { \"title\": \"?0\"}},\n" +
            "        { \"match\": { \"description\":  \"?0\" }},\n" +
            "        { \"match\": { \"category_name\": \"?0\"}}\n" +
            "      ]\n" +
            "    }\n" +
            "  }")
    // 上面使用@Query注解标记了要运行的查询语句,此情况下,当前方法的方法名就没有特定格式要求了
    // 在查询语句中,需要我们传参的位置使用?0,?1,?2.....这样的写法来占位
    // 在方法的参数列表中,按参数顺序依次赋值
    // 因为我们搜索spu商品的逻辑中,4个字段查询的是同一个分词,4个位置都写?0,传参只传一个即可
  //↓↓↓↓
    Page<SpuForElastic> querySpuSearch(
            //             ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            String keyword, Pageable pageable);

}






