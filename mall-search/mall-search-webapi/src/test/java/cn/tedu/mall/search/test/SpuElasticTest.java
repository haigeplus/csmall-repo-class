package cn.tedu.mall.search.test;


import cn.tedu.mall.pojo.search.entity.SpuForElastic;
import cn.tedu.mall.search.repository.SpuForElasticRepository;
import cn.tedu.mall.search.service.ISearchService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

// SpringBoot环境下得测试类,必须添加下面注解
@SpringBootTest
public class SpuElasticTest {

    @Autowired
    private ISearchService searchService;

    @Test
    void loadData(){
        searchService.loadSpuByPage();
        System.out.println("ok");
    }

    @Autowired
    private SpuForElasticRepository spuRepository;

    @Test
    void getAll(){
        Iterable<SpuForElastic> spus=spuRepository.findAll();
        spus.forEach(spu -> System.out.println(spu));
    }

    @Test
    void getTitle(){
        List<SpuForElastic> list=spuRepository
                .querySpuForElasticsByTitleMatches("手机华为");
        list.forEach(spu -> System.out.println(spu));
    }

    @Test
    void querySearch(){
        Page<SpuForElastic> page= spuRepository
                .querySpuSearch("手机", PageRequest.of(1,2));
        //List<SpuForElastic> list=spuRepository
        //       .querySpuSearch("手机");
        page.forEach(spu -> System.out.println(spu));
    }


}






